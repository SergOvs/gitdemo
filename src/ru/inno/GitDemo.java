package ru.inno;

/**
 * Класс, делающий что-то важное
 *
 * @author Овсянников Сергей Юрьевич
 * @since 04.02.2019
 */
public class GitDemo {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        System.out.println("Slave A");
        System.out.println("Slave B");
        System.out.println("Slave C");
        System.out.println("Slave D");
    }
}
